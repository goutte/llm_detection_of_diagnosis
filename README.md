# LLM_detection_of_diagnosis

This is the public repository for the article "Use of Large Language model for the detection of diagnosis in electronic health record: the case of gout in French medical documents" by Nils Bürgisser, Etienne Chalot, Samia Mehouachi, Kim Lauper, Delphine Courvoisier and Denis Mongin.

The repository contains the following elements:

- [classify_goutte.py](classify_goutte.py) is the python script to perform the LLM evaluation. For our study, it has been done on a NVIDIA Titan X with 12 GB of VRam, in conjunction with 20G of RAM standard CPU. The prompt are found in the file [prompt.py](prompt.py). 
- the folder results contains the csv files with the result providved by the LLM. The initial phrases stemming from the patients' EHR is not provided.
- analysis.R is the R file performing the analysis of the result, and producing the tables and figures of the article.
- The [gold_standard.csv](gold_standard.csv) file contains the gold standrad for the evaluated sentences.
- The csv file [parameters.csv](parameters.csv) contains the different parameters used for the semsitivity analysis. As the calculation as been performed with multijobs on a calculation cluster, the variable `input` range from 1 to 23 and perform the LLM evaluation for a given set of parameter, provided by the parameters file.
The `prompt` column follows the following coding:

|||
|---|---|
|0	|few shot with context, chain of thoughts and 4 categories output|
|1	|One shot with context, chain of thoughts and 4 output categories|
|2	|Reference (few shot with context, chain of thoughts and 2 output categories)|
|3	|Zero shot with no context, chain of thoughts and 4 output categories|
|4	|Zero shot with context, chain of thoughts and 4 output categories|
|5	|zero shot with context, no Chain of thoughts and 4 output categories|
|6	|few shot with context, no Chain of thoughts and 4 output categories|










