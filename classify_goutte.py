print("loading libraries")
from transformers import AutoTokenizer
import pandas as pd
import transformers
import torch
import numpy as np
import os
import sys
import gc
import datetime


# input for different simulation jobs
input = int(sys.argv[1])-1

###### load data #######
print("loading data")
# read data
full_data = pd.read_csv("forllama.csv", encoding='utf-8')
# parameters
params = pd.read_csv("parameters.csv", encoding='utf-8')
# read prompts
prompt_file = "prompts.py"
exec(open(prompt_file,encoding="utf-8").read())
preprompt = promp_list[params.loc[input,"prompt"]]

# assign prompt format
prompt_format="""<|start_header_id|>user<|end_header_id|>
Q: "{document_context}"<|eot_id|><|start_header_id|>assistant<|end_header_id|>
R:
"""
# without chain of thoughts
if(params.loc[input,"prompt"] >= 5): 
   prompt_format="""<|start_header_id|>user<|end_header_id|>
   Q: "{document_context}"<|eot_id|><|start_header_id|>assistant<|end_header_id|>
   A:
   """
   
# number of sequences
sequence_num = 1

##### output variables ########
print("create output")
# create results dataframe
tmp = full_data.assign(result = "")
# repeat each line number of sequence
result = pd.concat([tmp]*sequence_num)

# output filename
filename = "./results/result_llama3_8b_prompt_" + str(params.loc[input,"prompt"]) + "_temp_" + str(params.loc[input,"temp"]) + "_pen_" + str(params.loc[input,"penalty"]) + ".csv"

# read already done files
if os.path.exists(filename):
    done = pd.read_csv(filename)
    print(str(len(done))+" already done. Removing")
    # subset
    full_data = full_data.loc[~full_data.uniqueid.isin(done.uniqueid)].reset_index()

####### classifier ######
# load classifier
print("loading classifier")
# model = "meta-llama/Meta-Llama-3-8B"
model = "alokabhishek/Meta-Llama-3-8B-Instruct-bnb-8bit"
tokenizer = AutoTokenizer.from_pretrained(model)
pipeline = transformers.pipeline(
    "text-generation",
    model=model,
    torch_dtype=torch.float16,
    device_map="auto"
)

terminators = [
    pipeline.tokenizer.eos_token_id,
    pipeline.tokenizer.convert_tokens_to_ids("<|eot_id|>")
]

# funciton to flush meory
def flush():
  gc.collect()
  torch.cuda.empty_cache()
  torch.cuda.reset_peak_memory_stats()

##### do analysis #########3
print("start classify")
for i in full_data.index: # loop on all lines of data frame
  
  # log
  plouf = datetime.datetime.now() 
  print(plouf.strftime("%Y-%m-%d %Hh-%Mm-%Ss") + " : " + str(i) + " in " + str(len(full_data)))

  # sentence to classify
  sequence_to_classify = full_data.loc[i,"phrase_anonym"]
  entire_prompt = preprompt + prompt_format.format(document_context=sequence_to_classify)

  sequences = pipeline(
        entire_prompt,
        do_sample=True,
        top_k=40,
        top_p=0.95,
        repetition_penalty = params.loc[input,"penalty"],
        temperature = params.loc[input,"temp"],
        num_return_sequences = sequence_num,
        eos_token_id = terminators,
        max_new_tokens = 100,
    )
  # create a 1 line dataframe
  sub = result.loc[result["uniqueid"] == full_data.iloc[i,1]]
  # fill with the sequence results
  for j in range(0,sub.shape[0]):
    sub.iloc[j,3] = sequences[j]['generated_text'].replace(entire_prompt,"")
  # write to csv
  sub.to_csv(filename, mode='a' , header=not os.path.exists(filename))
  flush()
